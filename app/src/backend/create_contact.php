<?php

	header('Content-Type:text/plain');

	# Requiring the DB Connection
	try {
		require '../lib/conn.php';
	} catch (Exception $err) {
		header('Location:/contacts/create.php?error=DB');
		die('Error: Connecting to the DB' . PHP_EOL . $err);
	}

	$name = $_POST['fname'];
	$reg_code = $_POST['regcode'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];

	session_start();
	$user = $_SESSION['user'];
	

	$name = strtolower($name);
	$name = str_replace(['á', 'à'], 'a', $name);
	$name = str_replace(['í', 'ì'], 'i', $name);
	$name = str_replace(['ú', 'ù'], 'u', $name);
	$name = str_replace(['é', 'è'], 'e', $name);
	$name = str_replace(['ó', 'ò'], 'o', $name);
	$name = str_replace([ '.', ',', '+', '*', "'", '"', '<', '>', '{', '}', '[', ']', '´', '`' ], '' , $name);

	# Creating the contact
	$sql = $conn -> prepare("INSERT INTO contacts (user_id, full_name) VALUES (:user, :name) RETURNING id");
	$res = $sql  -> execute(array('user' => $user, 'name' => $name));
	$res = $sql -> fetchAll();
	$contact_id = $res[0]['id'];

	if (count($res) == 0) {
		header('Location:/contacts/create.php?error=CREATE');
		die('Error: Saving data' . PHP_EOL . 'Contact not created try again later.');
	}

	if ($reg_code != '' && $phone != '') {
		if (!preg_match('/^(\+|\-|)[0-9]{2}[0-9]{0,2}$/i', $reg_code)) {
			header('Location:/contacts/create.php?error=VALIDATIONREG');
			die('Error: Validating data' . PHP_EOL . 'Regional code is invalid.');
		}

		if (!preg_match('/^[0-9]+$/i', $phone)) {
			header('Location:/contacts/create.php?error=VALIDATIONPHO');
			die('Error: Validating data' . PHP_EOL . 'Phone number is invalid.');
		}

		$sql = $conn -> prepare("INSERT INTO contact_phones (contact, reg_code, phone) VALUES (:id, :reg, :phone)");
		$res = $sql  -> execute(array('id' => $contact_id, 'reg' => $reg_code, 'phone' => $phone));
	}

	if ($email != '') {
		if (!preg_match('/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i', $email)) {
			header('Location:/contacts/create.php?error=VALIDATIONEMA');
			die('Error: Validating data' . PHP_EOL . 'Email apd_dump_regular_resources() is invalid.');
		}

		$tmp = explode('@', $email);

		$sql = $conn -> prepare("INSERT INTO contact_emails (contact, user_name, host) VALUES (:id, :usr, :host)");
		$res = $sql  -> execute(array('id' => $contact_id, 'usr' => $tmp[0], 'host' => $tmp[1]));
	}

	header('Location:/contacts/view.php?id=' . $contact_id);
?>