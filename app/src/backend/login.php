<?php

	header('Content-Type:text/plain');

	# Requiring the DB Connection
	try {
		require '../lib/conn.php';
	} catch (Exception $err) {
		header('Location:/login.php?error=DB');
		die('Error: Connecting to the DB' . PHP_EOL . $err);
	}

	# Getting user & password
	$user = $_POST['uname'];
	$password = $_POST['password'];

	# Normalizing user & password
	# For Password
	$password = str_replace(['"', "'"], '', $password);
	$password = hash('sha512', $password);

	# For user
	$user = str_replace(['á', 'à'], 'a', $user);
	$user = str_replace(['í', 'ì'], 'i', $user);
	$user = str_replace(['ú', 'ù'], 'u', $user);
	$user = str_replace(['é', 'è'], 'e', $user);
	$user = str_replace(['ó', 'ò'], 'o', $user);
	$user = strtoupper($user);
	$user = str_replace(' ', '_', $user);
	$user = str_replace([ '+', '*', "'", '"', '<', '>', '{', '}', '[', ']', '´', '`' ], '' , $user);

	# Connect to the database for check data
	$sql = $conn -> prepare("SELECT id, password FROM login_users WHERE user_name=:user LIMIT 1");
	$res = $sql  -> execute(array('user' => $user));
	$res = $sql  -> fetchAll();

	if (count($res) == 0) {
		header('Location:/login.php?error=USER');
		die('Error: Getting the user' . PHP_EOL . 'The user doesn\'t exists.');
	}

	$res = $res[0];

	if ($res['password'] != $password) {
		header('Location:/login.php?error=PASSWORD');
		die('Error: Checking the password.' . PHP_EOL . 'The password doesn\'t match.');
	}

	session_start();

	$_SESSION['user'] = $res['id'];

	session_commit();

	header('Location:/');

?>