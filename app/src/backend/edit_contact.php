<?php

	header('Content-Type:text/plain');

	# Requiring the DB Connection
	try {
		require '../lib/conn.php';
	} catch (Exception $err) {
		header('Location:/contacts/edit.php?error=DB');
		die('Error: Connecting to the DB' . PHP_EOL . $err);
	}

	$contact_id = $_POST['id'];
	$reg_code = $_POST['regcode'];
	$phone = $_POST['phone'];
	$email = $_POST['email'];

	if ($reg_code != '' && $phone != '') {
		if (!preg_match('/^(\+|\-|)[0-9]{2}[0-9]{0,2}$/i', $reg_code)) {
			header('Location:/contacts/edit.php?error=VALIDATIONREG');
			die('Error: Validating data' . PHP_EOL . 'Regional code is invalid.');
		}

		if (!preg_match('/^[0-9]+$/i', $phone)) {
			header('Location:/contacts/edit.php?error=VALIDATIONPHO');
			die('Error: Validating data' . PHP_EOL . 'Phone number is invalid.');
		}

		$sql = $conn -> prepare("INSERT INTO contact_phones (contact, reg_code, phone) VALUES (:id, :reg, :phone)");
		$res = $sql  -> execute(array('id' => $contact_id, 'reg' => $reg_code, 'phone' => $phone));
	}

	if ($email != '') {
		if (!preg_match('/^[a-zA-Z0-9.!#$%&\'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/i', $email)) {
			header('Location:/contacts/edit.php?error=VALIDATIONEMA');
			die('Error: Validating data' . PHP_EOL . 'Email apd_dump_regular_resources() is invalid.');
		}

		$tmp = explode('@', $email);

		$sql = $conn -> prepare("INSERT INTO contact_emails (contact, user_name, host) VALUES (:id, :usr, :host)");
		$res = $sql  -> execute(array('id' => $contact_id, 'usr' => $tmp[0], 'host' => $tmp[1]));
	}

	header('Location:/contacts/view.php?id=' . $contact_id);

?>