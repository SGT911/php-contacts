<?php

	header('Content-Type:text/plain');

	# Requiring the DB Connection
	try {
		require '../lib/conn.php';
	} catch (Exception $err) {
		header('Location:/contacts/create.php?error=DB');
		die('Error: Connecting to the DB' . PHP_EOL . $err);
	}

	$id = $_REQUEST['id'];

	$sql = $conn -> prepare("DELETE FROM contact_phones WHERE contact = :id");
	$res = $sql  -> execute(array('id' => $id));

	$sql = $conn -> prepare("DELETE FROM contact_emails WHERE contact = :id");
	$res = $sql  -> execute(array('id' => $id));

	$sql = $conn -> prepare("DELETE FROM contacts WHERE id = :id");
	$res = $sql  -> execute(array('id' => $id));

	header('Location:/');
?>