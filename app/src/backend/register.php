<?php

	header('Content-Type:text/plain');

	# Requiring the DB Connection
	try {
		require '../lib/conn.php';
	} catch (Exception $err) {
		header('Location:/register.php?error=DB');
		die('Error: Connecting to the DB' . PHP_EOL . $err);
	}

	# Getting user & password
	$user = $_POST['uname'];
	$password = $_POST['password'];
	$full_name = $_POST['fname'];

	# Normalizing user & password
	# For Password
	$password = str_replace(['"', "'"], '', $password);
	$password = hash('sha512', $password);

	# For user
	$user = strtolower($user);
	$user = str_replace(['á', 'à'], 'a', $user);
	$user = str_replace(['í', 'ì'], 'i', $user);
	$user = str_replace(['ú', 'ù'], 'u', $user);
	$user = str_replace(['é', 'è'], 'e', $user);
	$user = str_replace(['ó', 'ò'], 'o', $user);
	$user = strtoupper($user);
	$user = str_replace(',', '.', $user);
	$user = str_replace(' ', '_', $user);
	$user = str_replace([ '+', '*', "'", '"', '<', '>', '{', '}', '[', ']', '´', '`' ], '' , $user);

	# For full name
	$full_name = strtolower($full_name);
	$full_name = str_replace(['á', 'à'], 'a', $full_name);
	$full_name = str_replace(['í', 'ì'], 'i', $full_name);
	$full_name = str_replace(['ú', 'ù'], 'u', $full_name);
	$full_name = str_replace(['é', 'è'], 'e', $full_name);
	$full_name = str_replace(['ó', 'ò'], 'o', $full_name);
	$full_name = str_replace([ '.', ',', '+', '*', "'", '"', '<', '>', '{', '}', '[', ']', '´', '`' ], '' , $full_name);

	# Checking if the user already exists.
	$sql = $conn -> prepare("SELECT id FROM login_users WHERE user_name = :user");
	$res = $sql  -> execute(array('user' => $user));
	$res = $sql  -> fetchAll();

	if (count($res) > 1) {
		header('Location:/register.php?error=USER');
		die('Error: Setting the user' . PHP_EOL . 'The user already exists.');
	}

	# Crating the user in the DB
	$sql = $conn -> prepare("INSERT INTO users (user_name, full_name, password) VALUES (:user, :fname, :password) RETURNING id");
	$res = $sql  -> execute(array('user' => $user, 'fname' => $full_name, 'password' => $password));
	$res = $sql -> fetchAll();

	if (count($res) == 0) {
		header('Location:/register.php?error=CREATE');
		die('Error: Saving data' . PHP_EOL . 'User not created try again later.');
	}

	session_start();

	$_SESSION['user'] = $res[0]['id'];

	session_commit();

	header('Location:/');

?>