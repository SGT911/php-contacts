<?php

	header('content-type:application/json');
	session_start();

	function get_contacts() {
		try {
			require '../lib/conn.php';
		} catch (Exception $err) {
			return array('error' => 'Can\'t connect to the Database', 'msg' => $err);
		}

		if ($_SESSION['user'] == null) {
			return array('error' => 'The user is not setted.');
		}

		$where = '';

		if ($_POST['search'] != null || $_POST['search'] != '') {
			$where = "WHERE contact_full_name ILIKE '%" . str_replace(' ', '%', $_POST['search']) . "%'";
		}

		$sql = $conn -> prepare("SELECT * FROM short_contacts $where");
		$res = $sql  -> execute();
		$res = $sql  -> fetchAll();

		for ($i=0; $i < count($res); $i++) { 
			$res[$i]['creation_date'] = (array) json_decode($res[$i]['creation_date']);
			$res[$i]['creation_time'] = (array) json_decode($res[$i]['creation_time']);
		}

		return $res;
	}

	echo json_encode(get_contacts(), JSON_PRETTY_PRINT);

?>