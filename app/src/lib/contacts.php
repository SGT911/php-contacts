<?php

	function to_array($obj) {
		if (is_object($obj)) {
			return (array) $obj;
		} else if (is_array($obj)) {
			foreach ($obj as $key => $value) {
				$obj[$key] = to_array($value);
				if (is_array($obj[$key])) {
					$obj[$key] = to_array($obj[$key]);
				}
			}
		}

		return $obj;
	}

	$ID = $_SESSION['user'];
	$type = ($type == null)? 'full' : $type;
	if ($type != 'short' && $type != 'short') {
		$type = 'full';
	}

	if ($ID == null ||$_SESSION == null) {
		header('Location:/error.php?error=INTERNAL_ID');
		die('Error: Getting the user' . PHP_EOL);
	}

	try {
		require 'conn.php';
	} catch (Exception $err) {
		header('Location:/error.php?error=DB');
		die('Error: Connecting to the DB' . PHP_EOL . $err);
	}

	$sql = $conn -> prepare('SELECT * FROM ' . $type . '_contacts WHERE user_id = :id');
	$res = $sql -> execute(array('id' => $ID));
	$res = $sql -> fetchAll();
	$len = $sql -> rowCount();
	$sql = null;

	for ($i=0; $i < $len; $i++) { 
		$res[$i]['creation_date'] = (array) json_decode($res[$i]['creation_date']);
		$res[$i]['creation_time'] = (array) json_decode($res[$i]['creation_time']);
		if ($type == 'full') {
			if ($res[$i]['contact_phones'] !== null) {
				$res[$i]['contact_phones'] = to_array(json_decode($res[$i]['contact_phones']));
			} else {
				$res[$i]['contact_phones'] = array();
			}
			if ($res[$i]['contact_emails'] !== null) {
				$res[$i]['contact_emails'] = to_array(json_decode($res[$i]['contact_emails']));
			} else {
				$res[$i]['contact_emails'] = array();
			}
		}
	}

	$CONTACTS = $res;

	$len = null;
	$res = null;
	$conn = null;
?>