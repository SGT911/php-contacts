<?php

	$conn = null;

	try {
		$conn = new PDO(
			'pgsql:user=' . $_ENV['DB_USERNAME'] .
			';password='  . $_ENV['DB_PASSWORD'] .
			';host='      . $_ENV['DB_HOST']     .
			';port='      . $_ENV['DB_PORT']     .
			';dbname='    . $_ENV['DB_DATABASE']
		);

		$conn -> setAttribute(
			PDO::ATTR_ERRMODE,
			PDO::ERRMODE_EXCEPTION
		);

		$conn -> setAttribute(
			PDO::ATTR_DEFAULT_FETCH_MODE,
			PDO::FETCH_ASSOC
		);

		$conn -> setAttribute(
			PDO::ATTR_EMULATE_PREPARES,
			false
		);
	} catch (PDOException $err) {
		$conn = null;

		throw $err;		
	}

?>