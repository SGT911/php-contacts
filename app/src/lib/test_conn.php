<?php

	header('Content-Type:text/plain');

	try {
		require './conn.php';
	} catch (Exception $e) {
		echo 'Error connecting to the DB.' . PHP_EOL;
		die($e);
	}

	echo "Connected succefully." . PHP_EOL;

	$sql = $conn -> prepare("SELECT 'Hello From PostgreSQL' as msg, version()");
	$res = $sql -> execute();
	$res = $sql -> fetchAll();

	echo  json_encode($res, JSON_PRETTY_PRINT);

?>