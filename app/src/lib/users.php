<?php

	$ID = $_SESSION['user'];

	if ($ID == null ||$_SESSION == null) {
		header('Location:/error.php?error=INTERNAL_ID');
		die('Error: Getting the user' . PHP_EOL);
	}

	try {
		require 'conn.php';
	} catch (Exception $err) {
		header('Location:/error.php?error=DB');
		die('Error: Connecting to the DB' . PHP_EOL . $err);
	}

	$sql = $conn -> prepare('SELECT * FROM full_users WHERE id = :id LIMIT 1');
	$res = $sql -> execute(array('id' => $ID));
	$res = $sql -> fetchAll();
	$len = $sql -> rowCount();
	$sql = null;

	if ($len == 0) {
		header('Location:/error.php?error=USER_404');
		die('Error: User not found' . PHP_EOL);
	} else {
		$res = $res[0];
		$res['creation_date'] = (array) json_decode($res['creation_date']);
		$res['creation_time'] = (array) json_decode($res['creation_time']);
	}

	$USER = $res;

	$len = null;
	$res = null;
	$conn = null;
?>