<?php

	$page = 'login.php';
	include './backend/must_have_user.php';

	include './lib/users.php';

	$type = 'short';
	include './lib/contacts.php';

	function complete_date($date) {
		$year = $date['year'];
		$month = $date['month'];
		$day = $date['day'];

		if ($month < 10) {
			$month = '0' . $month;
		}

		if ($day < 10) {
			$day = '0' . $day;
		}

		return "$year-$month-$day";
	}

	function complete_time($time) {
		$hour = $time['hour'];
		$minute = $time['minute'];
		$second = $time['second'];
		$tp = 'PM';

		if ($hour >= 12) {
			$tp = 'AM';

			if ($hour < 10) {
				$hour = '0' . $hour;	
			}
		}

		if ($minute < 10) {
			$minute = '0' . $minute;
		}

		if ($second < 10) {
			$second = '0' . $second;
		}

		return "$hour:$minute:$second $tp";
	}

	function len_contacts($contacts) {
		$res = array('contacts' => 0, 'emails' => 0, 'phones' => 0);

		$res['contacts'] = count($contacts);

		for ($i=0; $i < $res['contacts']; $i++) { 
			$res['phones'] += $contacts[$i]['contact_phones'];
			$res['emails'] += $contacts[$i]['contact_emails'];
		}

		return $res;
	}
?>
<!DOCTYPE html>
<html>
<head>
	<?php include './completions/meta.php'; ?>
	<title>PHP Contact - BY:SGT911</title>

	<?php include './completions/css.php'; ?>
	<link rel="stylesheet" href="/static/css/styles/cards.css">
	<link rel="stylesheet" href="/static/css/styles/index.css">
	<style>
		
	</style>
	<?php include './completions/js.php'; ?>
	<script src="/static/js/scripts/index.js" type="text/javascript" charset="utf-8" defer="true" async="true"></script>
</head>
<body>
	<div class="card top-margin">
		<div class="pure-g">
			<div class="pure-u-4-24">
				<img src="https://ui-avatars.com/api/?background=0D8ABC&color=fff&length=3&rounded=true&format=svg&size=128&name=<?php echo $USER['initials'] ?>" alt="USER">
			</div>
			<div class="pure-u-20-24">
				<h1 class="title"><?php echo $USER['full_name'] ?> (<?php echo $USER['user_name'] ?>)</h1>
				<div class="content pure-g">
					<div class="pure-u-12-24">
						<p>
							<b>User's ID:</b>
							<?php echo $ID ?>
						</p>
						<p>
							<b>Creation date:</b>
								<?php echo complete_date($USER['creation_date']) ?>
						</p>
						<p>
							<b>Creation time:</b>
								<?php echo complete_time($USER['creation_time']) ?>
						</p>
					</div>
					<div class="pure-u-12-24">
						<p>
							<b>Number of contacts:</b> <?php echo len_contacts($CONTACTS)['contacts'] ?>
						</p>
						<p>
							<b>Number of phones:</b> <?php echo len_contacts($CONTACTS)['phones'] ?>
						</p>
						<p>
							<b>Number of emails:</b> <?php echo len_contacts($CONTACTS)['emails'] ?>
						</p>
					</div>
				</div>	
			</div>
		</div>
	</div>
	<div class="card top-margin">
		<div class="pure-g">
			<div class="pure-u-4-24"></div>
			<div class="pure-u-14-24">
				<div class="search pure-g">
					<div class="pure-u-14-24">
						<input type="text" id="search-input" placeholder="Search a contact">
					</div>
					<div class="pure-u-2-24"></div>
					<div class="pure-u-4-24">
						<button id="search-button" class="pure-button pure-button-primary">Search</button>
					</div>
					<div class="pure-u-4-24">
						<button id="all-button" class="pure-button pure-button-secondary">Get All</button>
					</div>
					<div class="pure-u-1">
						<a href="/contacts/create.php" class="pure-button pure-button-secondary">Create a contact</a>
					</div>
					<div class="pure-u-1">
						<span id="founds"><?php echo len_contacts($CONTACTS)['contacts'] ?></span> contacts founds.
					</div>
				</div>
				<div id="user-list">
					<?php foreach ($CONTACTS as $key => $contact): ?>
						<div class="contact-card card pure-g">
							<div class="pure-u-3-24">
								<img src="https://ui-avatars.com/api/?background=087D7D&color=fff&length=3&rounded=true&format=svg&size=64&name=<?php echo $contact['initials'] ?>" alt="Avatar <?php echo $contact['contact_full_name'] ?>">
							</div>
							<div class="pure-u-21-24">
								<h1 class="title">
									<a href="/contacts/view.php?id=<?php echo $contact['contact_id'] ?>">
										<?php echo $contact['contact_full_name'] ?>
									</a>
								</h1>
								<div class="content pure-g">
									<div class="pure-u-12-24">
										<b>Number of phones:</b> <?php echo $contact['contact_phones'] ?>
									</div>
									<div class="pure-u-12-24">
										<b>Number of emails:</b> <?php echo $contact['contact_emails'] ?>
									</div>
								</div>
							</div>
						</div>
					<?php endforeach ?>
				</div>
			</div>
		</div>
	</div>
</body>
</html>