<?php

	$page = '';
	include '../backend/must_have_user.php';

	include '../lib/contacts.php';

	$CID = $_GET['id'];

	if ($CID == null) {
		header('Location:/error.php?error=CONTACT404');
		die('CID not found.');
	}

	function get_user($contacts, $id) {
		for ($i=0; $i < count($contacts); $i++) { 
			if($contacts[$i]['contact_id'] == $id) {
				return $contacts[$i];
			}
		}

		return null;
	}

	$contact = get_user($CONTACTS, $CID);

	if($contact == null) {
		header('Location:/error.php?error=CONTACT404');
		die('Contact not found.');
	}

	function complete_date($date) {
		$year = $date['year'];
		$month = $date['month'];
		$day = $date['day'];

		if ($month < 10) {
			$month = '0' . $month;
		}

		if ($day < 10) {
			$day = '0' . $day;
		}

		return "$year-$month-$day";
	}

	function complete_time($time) {
		$hour = $time['hour'];
		$minute = $time['minute'];
		$second = $time['second'];
		$tp = 'PM';

		if ($hour >= 12) {
			$tp = 'AM';

			if ($hour < 10) {
				$hour = '0' . $hour;	
			}
		}

		if ($minute < 10) {
			$minute = '0' . $minute;
		}

		if ($second < 10) {
			$second = '0' . $second;
		}

		return "$hour:$minute:$second $tp";
	}

?>


<!DOCTYPE html>
<html>
<head>
	<?php include '../completions/meta.php'; ?>
	<title>PHP Contact - <?php echo explode(' ', $contact['contact_full_name'])[0] ?></title>

	<?php include '../completions/css.php'; ?>
	<link rel="stylesheet" href="/static/css/styles/cards.css">
	<style>
		.hidden-link {
			color: rgba(0, 0, 0, 0.9);
			text-decoration: none;
		}

		.pure-u-11-24 > * {
			width: 100%;
		}
	</style>

	<?php include '../completions/js.php'; ?>
	<script src="/static/js/scripts/index.js" type="text/javascript" charset="utf-8" defer="true" async="true"></script>
</head>
<body>
	<div class="card top-margin">
		<div class="pure-g">
			<div class="pure-u-11-24">
				<a href="/" class="pure-button pure-button-primary">Go home</a>
			</div>
			<div class="pure-u-2-24"></div>
			<div class="pure-u-11-24">
				<a href="/contacts/edit.php?id=<?php echo $CID ?>" class="pure-button pure-button-secondary">Add numbers or emails</a>
			</div>
			<div class="pure-u-11-24">
				<a href="/contacts/create.php" class="pure-button pure-button-secondary">Create a contact</a>
			</div>
			<div class="pure-u-2-24"></div>
			<div class="pure-u-11-24">
				<a href="/contacts/delete.php?id=<?php echo $CID ?>" class="pure-button pure-button-error">Delete contact</a>
			</div>
		</div>
	</div>
	<div class="card top-margin">
		<div class="pure-g">
			<div class="pure-u-4-24">
				<img src="https://ui-avatars.com/api/?background=087D7D&color=fff&length=3&rounded=true&format=svg&size=128&name=<?php echo $contact['initials'] ?>" alt="CONTACT">
			</div>
			<div class="pure-u-20-24">
				<h1 class="title"> <?php echo $contact['contact_full_name'] ?> </h1>
				<div class="content">
					<p>
						<b>Contact's ID:</b>
						<?php echo $CID ?>
					</p>
					<p>
						<b>Creation date:</b>
							<?php echo complete_date($contact['creation_date']) ?>
					</p>
					<p>
						<b>Creation time:</b>
							<?php echo complete_time($contact['creation_time']) ?>
					</p>
				</div>	
			</div>
		</div>
	</div>
	<div class="card top-margin">
		<div class="pure-g">
			<div class="pure-u-11-24">
				<h1>Phones</h1>
				<?php foreach ($contact['contact_phones'] as $key => $phone): ?>
					<div>
						<hr class=separator>
						<p>
							<b>Creation date:</b>
								<?php echo complete_date($phone['date']) ?>
						</p>
						<p>
							<b>Creation time:</b>
								<?php echo complete_time($phone['time']) ?>
						</p>
						<p>
							<b>Phone:</b>
							<a class="hidden-link" href="callto:<?php echo str_replace(' ', '', $phone['phone']) ?>"><?php echo $phone['phone'] ?></a>
						</p>
					</div>
				<?php endforeach ?>
			</div>
			<div class="pure-u-2-24"></div>
			<div class="pure-u-11-24">
				<h1>Emails</h1>
				<?php foreach ($contact['contact_emails'] as $key => $email): ?>
					<div>
						<hr class=separator>
						<p>
							<b>Creation date:</b>
								<?php echo complete_date($email['date']) ?>
						</p>
						<p>
							<b>Creation time:</b>
								<?php echo complete_time($email['time']) ?>
						</p>
						<p>
							<b>Email:</b>
							<a class="hidden-link" href="mailto:<?php echo $email['email'] ?>"><?php echo $email['email'] ?></a>
						</p>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	</div>
</body>
</html>