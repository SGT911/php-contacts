<?php

	$page = '';
	include '../backend/must_have_user.php';	
	$ERROR = '';

	if (array_key_exists('error', $_GET)) {
		switch (strtoupper($_GET['error'])) {
			case 'DB':
				$ERROR = 'Connection to DB was failed.';
				break;

			case 'VALIDATIONREG':
				$ERROR = 'Regional code validation not satisfied.';
				break;

			case 'VALIDATIONPHO':
				$ERROR = 'Phone number validation not satisfied.';
				break;

			case 'VALIDATIONEMA':
				$ERROR = 'Email address validation not satisfied.';
				break;

			case 'CREATE':
				$ERROR = 'User not created, try again later.';
				break;
			
			default:
				$ERROR = 'Weird error :(<br>' . '"' . $_GET['error'] . '"';
				break;
		}
	}

?>
<!DOCTYPE html>
<html>
<head>
	<?php include '../completions/meta.php'; ?>
	<title>PHP Contact - Create User</title>

	<?php include '../completions/css.php'; ?>
	<link rel="stylesheet" type="text/css" href="/static/css/styles/form.css">
	<?php if (array_key_exists('error', $_GET)): ?>
		<link rel="stylesheet" type="text/css" href="/static/css/styles/alert.css">
	<?php endif ?>
	<?php include '../completions/js.php'; ?>
	<script src="/static/js/scripts/create_contact.js" defer="true" async="true"></script>
</head>
<body>
	<?php if (array_key_exists('error', $_GET)): ?>
		<div class="alert">
			<span>
				<b>Error:</b>
				<?php echo $ERROR ?>
			</span>
		</div>

		<div class="pure-g minor-margin">
	<?php else: ?>
		<div class="pure-g">
	<?php endif ?>
		<div class="pure-u-1-24 pure-u-sm-2-24 pure-u-lg-6-24 pure-u-xl-8-24"></div>
		<form action="/backend/create_contact.php" method="POST" id="form" class="pure-u-22-24 pure-u-sm-20-24 pure-u-lg-18-24 pure-u-xl-8-24 pure-form pure-form-aligned">
			<div class="form-header">
				<h1>Create An User</h1>
			</div>
			<div class="form-content">
				<div class="pure-control-group">
					<label for="fname">Full Name</label>
					<input type="text" name="fname" id="fname" required placeholder="Full Name">
				</div>
				<hr>
				<div class="pure-control-group">
					<label for="phone">Regional Code</label>
					<input type="text" inputmode="numeric" pattern="^(\+|\-|)[0-9]{2}[0-9]{0,2}$" name="regcode" id="regcode" placeholder="Regional code">
				</div>
				<div class="pure-control-group">
					<label for="phone">Phone Number</label>
					<input type="number" inputmode="numeric" name="phone" id="phone" placeholder="Phone Number">
				</div>
				<hr>
				<div class="pure-control-group">
					<label for="phone">Email address</label>
					<input type="email" inputmode="email" name="email" id="email" placeholder="Email address">
				</div>
			</div>
			<div class="form-footer pure-g">
				<div class="pure-u-24-24 pure-button-group" role="group" style="margin:0.6em;">
						<button class="pure-button pure-button-primary">Create contact</button>
				</div>
			</div>
		</form>
	</div>
</body>
</html>