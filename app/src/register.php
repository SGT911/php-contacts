<?php

	$is_loged = true;
	$page = '';
	include './backend/must_have_user.php';	
	$ERROR = '';

	if (array_key_exists('error', $_GET)) {
		switch (strtoupper($_GET['error'])) {
			case 'DB':
				$ERROR = 'Connection to DB was failed.';
				break;

			case 'USER':
				$ERROR = 'User exists, try with other.';
				break;

			case 'CREATE':
				$ERROR = 'User not created, try again later.';
				break;
			
			default:
				$ERROR = 'Weird error :(<br>' . '"' . $_GET['error'] . '"';
				break;
		}
	}

?>
<!DOCTYPE html>
<html>
<head>
	<?php include './completions/meta.php'; ?>
	<title>PHP Contact - Create Account</title>

	<?php include './completions/css.php'; ?>
	<link rel="stylesheet" type="text/css" href="/static/css/styles/form.css">
	<?php if (array_key_exists('error', $_GET)): ?>
		<link rel="stylesheet" type="text/css" href="/static/css/styles/alert.css">
	<?php endif ?>
	<?php include './completions/js.php'; ?>
	<script src="/static/js/scripts/register.js" defer="true" async="true"></script>
</head>
<body>
	<?php if (array_key_exists('error', $_GET)): ?>
		<div class="alert">
			<span>
				<b>Error:</b>
				<?php echo $ERROR ?>
			</span>
		</div>

		<div class="pure-g minor-margin">
	<?php else: ?>
		<div class="pure-g">
	<?php endif ?>
		<div class="pure-u-1-24 pure-u-sm-2-24 pure-u-lg-6-24 pure-u-xl-8-24"></div>
		<form action="/backend/register.php" method="POST" id="form" class="pure-u-22-24 pure-u-sm-20-24 pure-u-lg-18-24 pure-u-xl-8-24 pure-form pure-form-aligned">
			<div class="form-header">
				<h1>Create An Account</h1>
			</div>
			<div class="form-content">
				<div class="pure-control-group">
					<label for="uname">User Name</label>
					<input type="text" name="uname" id="uname" placeholder="User Name">
				</div>
				<div class="pure-control-group">
					<label for="fname">Full Name</label>
					<input type="text" name="fname" id="fname" placeholder="Full Name">
				</div>
				<div class="pure-control-group">
					<label for="password">Password</label>
					<input type="password" name="password" id="password" placeholder="Password">
					<div>
						<label for="password-checkbox"></label>
						<input type="checkbox" id="password-checkbox">
						<span id="password-checkbox-text">Show password.</span>
					</div>
				</div>
			</div>
			<div class="form-footer pure-g">
				<div class="pure-u-24-24">
					<div class="pure-button-group" role="group" style="margin:0.6em;">
						<button class="pure-button pure-button-primary">Create an Account</button>
						<a href="/login.php" class="pure-button pure-button-secondary">Login</a>
					</div>
				</div>
				<div class="pure-u-24-24">
					<div class="pure-button-group" role="group" style="margin-top:-1em;">
						<a href="javascript:void(0)" id="clear" class="pure-button pure-button-error">Clear form</a>
					</div>
				</div>
			</div>
		</form>
	</div>
</body>
</html>