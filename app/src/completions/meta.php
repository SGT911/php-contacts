<!-- META -->
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta http-equiv="Expires" content="0">
<meta http-equiv="Last-Modified" content="0">
<meta http-equiv="Cache-Control" content="no-cache, mustrevalidate">
<meta http-equiv="Pragma" content="no-cache">

<meta charset="utf-8">
<meta name="mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-capable" content="yes">

<meta name="application-name" content="PHP Contacts">
<meta name="apple-mobile-web-app-title" content="PHP Contacts">

<meta name="theme-color" content="#ccc">
<meta name="msapplication-navbutton-color" content="#ccc">
<meta name="apple-mobile-web-app-status-bar-style" content="#ccc">
<meta name="msapplication-starturl" content="/">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">