<?php

	if($_GET['error'] == null) {
		header('Location:/');
		die('No error query.');
	}

	$error =  array('name' =>  null,  'msg' => null);

	switch (strtoupper($_GET['error'])) {
		case 'DB':
			$error['name'] = 'Connection to the Database';
			$error['msg'] = 'Unknown error.';
			break;
		
		default:
			$error['name'] = strtoupper($_GET['error']);
			$error['msg'] = 'Unknown error, Check in the issue page of the repository "' . strtoupper($_GET['error']) . '"';
			break;
	}

?>

<!DOCTYPE html>
<html>
<head>
	<?php include './completions/meta.php'; ?>
	<title>PHP Contact - Error: <?php echo strtoupper($_GET['error']) ?> </title>

	<?php include './completions/css.php'; ?>
	<link rel="stylesheet" href="/static/css/styles/cards.css">
	<?php include './completions/js.php'; ?>
</head>
<body>
	<div class="card top-margin">
		<h1 class="title">Error: <?php echo $error['name'] ?></h1>
		<p class="content"><?php echo $error['msg'] ?></p>
		<hr class="separator">
		<a href="/" class="pure-button pure-button-error">Go to home.</a>
	</div>
</body>
</html>