'use strict'

const search_input = document.getElementById('search-input')
const search_button = document.getElementById('search-button')
const get_all_button = document.getElementById('all-button')
const founds_span = document.getElementById('founds')
const user_list = document.getElementById('user-list')

function create_contact_card(contact) {
	return `<div class="contact-card card pure-g">
				<div class="pure-u-3-24">
					<img src="https://ui-avatars.com/api/?background=087D7D&color=fff&length=3&rounded=true&format=svg&size=64&name=${contact.initials}" alt="Avatar ${contact.contact_full_name}">
				</div>
				<div class="pure-u-21-24">
					<h1 class="title">
						<a href="/contacts/view.php?id=${contact.contact_id}">
							${contact.contact_full_name}
						</a>
					</h1>
					<div class="content pure-g">
						<div class="pure-u-12-24">
							<b>Number of phones:</b> ${contact.contact_phones}
						</div>
						<div class="pure-u-12-24">
							<b>Number of emails:</b> ${contact.contact_emails}
						</div>
					</div>
				</div>
			</div>`
}

async function get_data(search) {
	let form = new FormData()
	form.append('search', search)

	let response = await fetch('/backend/search_contacts.php', {
		method: 'POST',
		mode: 'no-cors',
		body: form
	}).then(out => out.json())

	if (response.error) {
		alert(response.error)
		console.error(response)
		return false
	}

	founds_span.innerText = response.length

	user_list.innerHTML = ''

	response.forEach((obj) => {
		user_list.innerHTML += create_contact_card(obj)
	})
	
	return true
}

get_all_button.onclick = async (evt) => {
	return await get_data('')
}

search_button.onclick = async (evt) => {
	if (search_input.value == '') {
		alert('You most write something.')
		return false
	}

	return await get_data(search_input.value)
}