'use strict'

const form = document.getElementById('form')

const user = document.getElementById('uname')

const password = {
	input: document.getElementById('password'),
	is_hidden: document.getElementById('password-checkbox'),
	is_hidden_text: document.getElementById('password-checkbox-text')
}

const clear_btn = document.getElementById('clear')

// Form check empty
form.onsubmit = (evt) => {
	if (user.value == '' || password.value == '') {
		return false
	}

	return true
}

// User input filtering & normalizing
user.oninput = (evt) => {
	evt.target.value = evt.target.value.replace(/[\á\à]/, 'a')
	evt.target.value = evt.target.value.replace(/[\í\ì]/, 'i')
	evt.target.value = evt.target.value.replace(/[\ú\ù]/, 'u')
	evt.target.value = evt.target.value.replace(/[\é\è]/, 'e')
	evt.target.value = evt.target.value.replace(/[\ó\ò]/, 'o')
	evt.target.value = evt.target.value.toUpperCase()
	evt.target.value = evt.target.value.replace(/\ /, '_')
	evt.target.value = evt.target.value.replace(/\,/, '.')
	evt.target.value = evt.target.value.replace(/[\+\*\'\"\<\>\{\}\[\]\´\`]+/ig, '')
}

// Clear button
clear.onclick = (ext) => {
	user.value = ''
	password.input.value = ''

	return false
}

// Password checkbox for Hide/Unhide input
password.is_hidden_text.style['user-select'] = 'none'
password.is_hidden_text.onclick = (evt) => {
	password.is_hidden.checked = !password.is_hidden.checked
	password.is_hidden.oninput({target: password.is_hidden})
}

password.is_hidden.checked = false
password.is_hidden.oninput = (evt) => {
	if (evt.target.checked == true) {
		password.input.type = 'text'
	} else {
		password.input.type = 'password'
	}
}