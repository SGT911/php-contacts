'use strict'

const name = document.getElementById('fname')

// Full name normalizing & format
name.oninput = (evt) => {
	evt.target.value = evt.target.value.toLowerCase()
	evt.target.value = evt.target.value.replace(/[\á\à]/, 'a')
	evt.target.value = evt.target.value.replace(/[\í\ì]/, 'i')
	evt.target.value = evt.target.value.replace(/[\ú\ù]/, 'u')
	evt.target.value = evt.target.value.replace(/[\é\è]/, 'e')
	evt.target.value = evt.target.value.replace(/[\ó\ò]/, 'o')
	evt.target.value = evt.target.value.replace(/\_/, ' ')
	evt.target.value = evt.target.value.split(/\ /).map((obj) => {
		if (obj.length <= 1) {
			return obj.toUpperCase()
		}

		return obj.substr(0, 1).toUpperCase() + obj.substr(1).toLowerCase()
	}).join(' ')
	evt.target.value = evt.target.value.replace(/[\,\.\+\*\'\"\<\>\{\}\[\]\´\`]+/ig, '')
}