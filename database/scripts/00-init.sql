--------------------
--    Struture    --
--------------------

CREATE SEQUENCE id_users_seq START 1 INCREMENT BY 1;
CREATE TABLE users (
	id INT NOT NULL DEFAULT nextval('id_users_seq'::regclass),
	cration_date DATE NOT NULL DEFAULT NOW()::DATE,
	cration_time TIME NOT NULL DEFAULT NOW()::TIME,
	user_name VARCHAR(16) NOT NULL,
	full_name VARCHAR(150) NOT NULL,
	password VARCHAR(299) NOT NULL,
	PRIMARY KEY(id),
	UNIQUE (user_name)
);

CREATE SEQUENCE id_contacts_seq START 1 INCREMENT BY 1;
CREATE TABLE contacts (
	id INT NOT NULL DEFAULT nextval('id_contacts_seq'::regclass),
	user_id INT NOT NULL,
	cration_date DATE NOT NULL DEFAULT NOW()::DATE,
	cration_time TIME NOT NULL DEFAULT NOW()::TIME,
	full_name VARCHAR(150),
	PRIMARY KEY (id),
	FOREIGN KEY (user_id) REFERENCES users(id)
);

CREATE SEQUENCE id_contacts_phones_seq START 1 INCREMENT BY 1;
CREATE TABLE contact_phones (
	id INT NOT NULL DEFAULT nextval('id_contacts_phones_seq'::regclass),
	contact INT NOT NULL,
	cration_date DATE NOT NULL DEFAULT NOW()::DATE,
	cration_time TIME NOT NULL DEFAULT NOW()::TIME,
	reg_code CHAR(5) NULL,
	phone BIGINT NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (contact) REFERENCES contacts(id)
);

CREATE SEQUENCE id_contacts_emails_seq START 1 INCREMENT BY 1;
CREATE TABLE contact_emails (
	id INT NOT NULL DEFAULT nextval('id_contacts_emails_seq'::regclass),
	contact INT NOT NULL,
	cration_date DATE NOT NULL DEFAULT NOW()::DATE,
	cration_time TIME NOT NULL DEFAULT NOW()::TIME,
	user_name VARCHAR(60) NOT NULL,
	host VARCHAR(60) NOT NULL,
	PRIMARY KEY (id),
	FOREIGN KEY (contact) REFERENCES contacts(id)
);