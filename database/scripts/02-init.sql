--------------------
--      Views     --
--------------------

-- Users
CREATE VIEW login_users AS
	SELECT 
		id,
		user_name AS user_name,
		password
	FROM users;

CREATE VIEW full_users AS
	SELECT 
		id, 
		user_name AS user_name,
		INITCAP(full_name) AS full_name,
		GET_NAME_INITIALS(full_name) AS initials,
		('{' ||
			'"year": ' || EXTRACT(YEAR FROM cration_date) ||
		 	', "month": ' || EXTRACT(MONTH FROM cration_date) ||
		 	', "day": ' || EXTRACT(DAY FROM cration_date) ||
		'}')::JSON AS creation_date,
		('{' ||
			'"hour": ' || EXTRACT(HOUR FROM cration_time) ||
		 	', "minute": ' || EXTRACT(MINUTE FROM cration_time) ||
		 	', "second": ' || (EXTRACT(SECOND FROM cration_time))::INT ||
		'}')::JSON AS creation_time
	FROM users;

-- Contacts
CREATE VIEW full_contacts AS
	SELECT
		contacts.user_id AS user_id,
		contacts.id AS contact_id,
		INITCAP(contacts.full_name) AS contact_full_name,
		GET_NAME_INITIALS(full_name) AS initials,
		('{' ||
			'"year": ' || EXTRACT(YEAR FROM contacts.cration_date) ||
		 	', "month": ' || EXTRACT(MONTH FROM contacts.cration_date) ||
		 	', "day": ' || EXTRACT(DAY FROM contacts.cration_date) ||
		'}')::JSON AS creation_date,
		('{' ||
			'"hour": ' || EXTRACT(HOUR FROM contacts.cration_time) ||
		 	', "minute": ' || EXTRACT(MINUTE FROM contacts.cration_time) ||
		 	', "second": ' || (EXTRACT(SECOND FROM contacts.cration_time))::INT ||
		'}')::JSON AS creation_time,
		(
			SELECT '[' || STRING_AGG(
				('{' ||
					 '"id": ' || cphones.id || 
					 ', "phone": "' || trim(cphones.reg_code) || ' ' || cphones.phone || '"' ||
					 ', "date": {' ||
					 	'"year": ' || EXTRACT(YEAR FROM cphones.cration_date) ||
					 	', "month": ' || EXTRACT(MONTH FROM cphones.cration_date) ||
					 	', "day": ' || EXTRACT(DAY FROM cphones.cration_date) ||
					 '}' ||
					 ', "time": {' ||
					 	'"hour": ' || EXTRACT(HOUR FROM cphones.cration_time) ||
					 	', "minute": ' || EXTRACT(MINUTE FROM cphones.cration_time) ||
					 	', "second": ' || (EXTRACT(SECOND FROM cphones.cration_time))::INT ||
					 '}' ||
				'}')::TEXT,
				', '
			) || ']' AS contact_phones
			FROM contact_phones AS cphones
				WHERE cphones.contact = contacts.id
		)::JSON AS contact_phones,
		(
			SELECT '[' || STRING_AGG(
				('{' || 
					'"id": ' || cemails.id ||
					', "email": "' || cemails.user_name || '@' || cemails.host || '"' || 
					', "date": {' ||
					 	'"year": ' || EXTRACT(YEAR FROM cemails.cration_date) ||
					 	', "month": ' || EXTRACT(MONTH FROM cemails.cration_date) ||
					 	', "day": ' || EXTRACT(DAY FROM cemails.cration_date) ||
					 '}' ||
					 ', "time": {' ||
					 	'"hour": ' || EXTRACT(HOUR FROM cemails.cration_time) ||
					 	', "minute": ' || EXTRACT(MINUTE FROM cemails.cration_time) ||
					 	', "second": ' || (EXTRACT(SECOND FROM cemails.cration_time))::INT ||
					 '}' ||
				'}')::TEXT,
				', '
			) || ']' AS contact_emails
			FROM contact_emails AS cemails
				WHERE cemails.contact = contacts.id
		)::JSON AS contact_emails
		FROM contacts
		GROUP BY contacts.id, contacts.id, contacts.full_name, contacts.cration_date, contacts.cration_time
		ORDER BY contact_full_name;

CREATE VIEW short_contacts AS
	SELECT
		contacts.user_id AS user_id,
		contacts.id AS contact_id,
		INITCAP(contacts.full_name) AS contact_full_name,
		GET_NAME_INITIALS(full_name) AS initials,
		('{' ||
			'"year": ' || EXTRACT(YEAR FROM contacts.cration_date) ||
		 	', "month": ' || EXTRACT(MONTH FROM contacts.cration_date) ||
		 	', "day": ' || EXTRACT(DAY FROM contacts.cration_date) ||
		'}')::JSON AS creation_date,
		('{' ||
			'"hour": ' || EXTRACT(HOUR FROM contacts.cration_time) ||
		 	', "minute": ' || EXTRACT(MINUTE FROM contacts.cration_time) ||
		 	', "second": ' || (EXTRACT(SECOND FROM contacts.cration_time))::INT ||
		'}')::JSON AS creation_time,
		(
			SELECT 
				COUNT(cphones.id) AS contact_phones
			FROM contact_phones AS cphones
				WHERE cphones.contact = contacts.id
		) AS contact_phones,
		(
			SELECT 
				COUNT(cemails.id) AS contact_emails
			FROM contact_emails AS cemails
				WHERE cemails.contact = contacts.id
		) AS contact_emails
		FROM contacts
		GROUP BY contacts.id, contacts.id, contacts.full_name, contacts.cration_date, contacts.cration_time
		ORDER BY contact_full_name;