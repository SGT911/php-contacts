--------------------
--    Functions   --
--------------------

CREATE OR REPLACE FUNCTION GET_NAME_INITIALS(full_name TEXT)
RETURNS TEXT AS $$
DECLARE 
    result TEXT :='';
    part VARCHAR :='';
BEGIN
    FOREACH part IN ARRAY STRING_TO_ARRAY($1, ' ') LOOP
        result :=  result || SUBSTR(UPPER(part), 1, 1);
    END LOOP;

    RETURN result;
END;
$$ LANGUAGE plpgsql;