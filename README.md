# PHP Contacts
## By SGT911

This Application is with the purpose to study code with PHP & PostgreSQL.
You can create a branch of this project and create your own variants or add features to the project

This software can be used only with didactic purposes.

## Install
PHP Contacts is runing on docker and can be installed on a Linux server using the software "GNU Make".
prerequisite:
+ GNU Make
+ Docker

### Build docker images
```bash
make build
```
### Generate a run dev file
```bash
make dev_run
```

## Technologies
+ PHP (Vanilla)
+ CSS (PureCSS)
+ JS (Vanilla)
+ DB (PostgreSQL)
+ Avatar Creator ([UI Avatar](https://ui-avatars.com/))