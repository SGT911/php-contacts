# Environment
# Build for docker
PREFIX=sgt911

#Procedures
.PHONY: build

# Build for docker
build: build_db build_app

build_db:
	@echo "Building DB of PHP App"
	docker build --rm --no-cache -t ${PREFIX}/contacts-db ./database

build_app:
	@echo "Building PHP App"
	docker build --rm --no-cache -t ${PREFIX}/contacts-app:php ./app

# Dev run environment
dev_run:
	@echo "Creating run file"
	echo -e "#!/bin/bash\n\n\ndocker rm -f contacts_database_1 contacts_app_1\ndocker-compose up --build --remove-orphans" > run
	chmod +x run